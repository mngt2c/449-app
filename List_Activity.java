package com.example.mnggi.restaurant_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class List_Activity extends AppCompatActivity {

    ListView listView;

    SharedPreferences shpref;

    ArrayList<Restaurants> saveList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_);

        //finding listview
        listView = findViewById(R.id.listview);

        shpref = getSharedPreferences("Rests", Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String response = shpref.getString("key", "");

        saveList = gson.fromJson(response, new TypeToken<List<Restaurants>>(){}.getType());

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),activity_listdata.class);
                intent.putExtra("name",saveList.get(i).getName());
                intent.putExtra("address",saveList.get(i).getAddress());
                intent.putExtra("price",saveList.get(i).getPrice());
                intent.putExtra("type", saveList.get(i).getFood_type());
                startActivity(intent);

            }
        });


    }

    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return saveList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_data,null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.names);

            name.setText(saveList.get(i).getName());
            return view1;



        }
    }
}