package com.example.mnggi.restaurant_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Random_Activity extends AppCompatActivity {

    TextView name;
    TextView address;
    TextView price;
    TextView type;

    SharedPreferences shpref;

    ArrayList<Restaurants> saveList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_);

        shpref = getSharedPreferences("Rests", Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String response = shpref.getString("key", "");

        saveList = gson.fromJson(response, new TypeToken<List<Restaurants>>(){}.getType());

        Random rand = new Random();
        int randInt = rand.nextInt(saveList.size());

        Restaurants selected = saveList.get(randInt);

        name = findViewById(R.id.Name);
        address = findViewById(R.id.Address);
        price = findViewById(R.id.Price);
        type = findViewById(R.id.Type);
        Intent intent = getIntent();
        String receivedName =  selected.getName();
        String receivedAddress = selected.getAddress();
        String receivedPrice = selected.getPrice();
        String receivedType = selected.getFood_type();

        name.setText(receivedName);
        address.setText(receivedAddress);
        price.setText(receivedPrice);
        type.setText(receivedType);


    }
}
