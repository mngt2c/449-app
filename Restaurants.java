package com.example.mnggi.restaurant_app;

import java.io.Serializable;

public class Restaurants implements Serializable {
    private String name, address, price, food_type;
    public Restaurants(String newName, String newAddress, String newPrice, String newFood_type){
            name = newName;
            address = newAddress;
            price = newPrice;
            food_type = newFood_type;
        }
    public String getName(){
            return name;
        }
    public String getAddress(){
            return address;
        }
    public String getPrice(){
            return price;
        }
    public String getFood_type(){
            return food_type;
        }
}
